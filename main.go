package main

// Import required packages for this project
import (
	"context"
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	// K8's Libraries to create a structured response
	"k8s.io/api/admission/v1beta1"
	k8sapiv1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const port = "8080"

var cert, key string

// Admission - base admission struct
type Admission struct {
	Admit bool `json:"bool"`
}

// Helper function to construct a response. Accepts an
// admission decision and event message
func testAdmission(allow bool, msg string) v1beta1.AdmissionReview {
	testAdmit := v1beta1.AdmissionReview{
		Response: &v1beta1.AdmissionResponse{
			Allowed: allow,
			Result: &k8sapiv1.Status{
				Message: msg,
			},
		},
	}
	return testAdmit
}

// Helper function to craft a query. Accepts a request
// and parameter string as input
func testRequest(req string, params string) (*http.Response, error) {

	// Construct an HTTP client
	resp, err := http.Get("https://" + req + "/" + params)
	if err != nil {
		log.Fatalln(err)
	}
	return resp, nil
}

// Helper function to construct Admission query
func getAdmission(w http.ResponseWriter, r *http.Request) {

	resp, err := testRequest("admission.default.svc", "v1/admit")
	if err != nil {
		fmt.Printf("Something went wrong: %v", err)
	}
	defer resp.Body.Close()

	// Upon successful request, parse body of web response
	if resp.StatusCode == 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}

		/* --- Primary business logic --- */
		testres := &Admission{}
		// Unmarshal the incoming JSON into the admission structure
		testerr := json.Unmarshal([]byte(body), testres)
		if testerr != nil {
			log.Fatal(testerr)
		}

		// If Admission is true, let 'er through
		if testres.Admit == true {
			if _, err := w.Write(jsonMarshal(true, "Go ahead")); err != nil {
				fmt.Printf("Something went wrong: %v", err)
			}
			return
		}
		// If Admission is false, ..don't
		if testres.Admit == false {
			if _, err := w.Write(jsonMarshal(false, "None shall pass")); err != nil {
				fmt.Printf("Something went wrong: %v", err)
			}
			return
		}
		// Handle unsuccessful API call
	} else {
		fmt.Printf("Unable to GET - API responded with an HTTP/%d", resp.StatusCode)
		os.Exit(1)
	}
}

func jsonMarshal(allow bool, msg string) []byte {
	// Marshal JSON into objects
	validate, err := json.Marshal(testAdmission(allow, msg))
	if err != nil {
		fmt.Printf("Failed to encode response: %v", err)
	}
	return validate
}

func main() {
	// Set up TLS cert/key file locations
	flag.StringVar(&cert, "tlsCertFile", "/etc/certs/cert.pem", "The certificate file.")
	flag.StringVar(&key, "tlsKeyFile", "/etc/certs/key.pem", "The key file")
	flag.Parse()
	
    // Load these into a certs object
	certs, err := tls.LoadX509KeyPair(cert, key)
	if err != nil {
		fmt.Printf("Failed to load key pair: %v\n", err)
	}

	// Define a server object
	server := &http.Server{
		Addr:      fmt.Sprintf(":%v", port),
		TLSConfig: &tls.Config{Certificates: []tls.Certificate{certs}},
	}

	// Define endpoints and start server
	mux := http.NewServeMux()
	mux.HandleFunc("/admission", getAdmission)
	server.Handler = mux

	// Start new go routine for web server
	go func() {
		if err := server.ListenAndServeTLS("", ""); err != nil {
			fmt.Printf("Failed to listen and serve webhook server: %v\n", err)
		}
	}()

	// Log status message to stdout
	fmt.Printf("Server running listening on port: %v\n", port)

	// Listen for shutdown
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	// Emit a shutdown log
	fmt.Println("Got shutdown signal, shutting down webhook server gracefully...")
	server.Shutdown(context.Background())
}
