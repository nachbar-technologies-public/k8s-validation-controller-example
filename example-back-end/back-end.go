package main

import (
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

func admit(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(`{"Admit": true}`))
}

func deny(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(`{"Admit": false}`))
}

func main() {
    r := mux.NewRouter()

    api := r.PathPrefix("/api/v1").Subrouter()
	api.HandleFunc("/admit", admit).Methods(http.MethodGet)
	api.HandleFunc("/deny", deny).Methods(http.MethodGet)

    log.Fatal(http.ListenAndServe(":8080", r))
}
