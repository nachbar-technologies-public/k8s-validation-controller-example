#!/bin/bash

# Set up a few guardrails
set -o errexit
set -o nounset
set -o pipefail

# Usual process of CA generation
openssl genrsa -out certs/ca.key 4096
openssl req -new -x509 -key certs/ca.key -out certs/ca.crt -config certs/ca_config.txt
openssl genrsa -out certs/example-validation-key.pem 4096

# Simple generation of key pair. Don't use the default namespace in production.
openssl req -new -key certs/example-validation-key -subj "/CN=admission.default.svc" -out example-validation-CSR.csr -config certs/example-validation-settings.txt
openssl x509 -req -in example-validation-CSR.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -out certs/example-validation-certificate.pem

# Make a valid K8's manifest for upload. Will ultimately extend the API to query our controller
export CA_BUNDLE=$(cat certs/ca.crt | base64 | tr -d '\n')
sed "s/CA_BUNDLE/${CA_BUNDLE}/g" manifest-generator.yaml > example-manifest.yaml
